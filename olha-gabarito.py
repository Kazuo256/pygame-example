
from sys import argv
from os import path

file_path = path.join("gabarito", "game.py")
match = argv[1]

print("Gabarito para passo %s" % match)
print("==============================")

with open(file_path) as search:
    line_num = 1
    for line in search:
        line = line.rstrip()  # remove '\n' at end of line
        if match in line:
            color = "\033[32m"
            if ("-[%s]" % match) in line:
                color = "\033[31m"
            print(color + ("%3d| " % line_num) + line)
        line_num += 1

