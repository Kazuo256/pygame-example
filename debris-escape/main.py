
import pygame
import os

from game import Game

FRAME = 1.0/60.0

# define a main function
def main():
     
    while True:
        game = Game()
        game.setup_all()
        game.create_entities()

        lag = 0.0
         
        # main loop
        while game.running:
            input_state = game.process_input()
            delta = game.process_time()
            if not game.paused:
                lag += delta
                while lag >= FRAME:
                    game.process_control(input_state, FRAME)
                    game.process_physics(FRAME)
                    lag -= FRAME
            game.draw_frame()
        game.audio_player.bgm.stop()
        if not game.retry:
            break
     
# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()
