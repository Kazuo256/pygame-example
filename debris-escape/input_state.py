
from pygame.math import Vector2

class InputState:
    exit_requested: bool = False
    pause_requested: bool = False
    shooting: bool = False
    boosting: bool = False
    steer: float = 0.0
    def __init__(self):
        self.move_dir = Vector2()
