
from dataclasses import dataclass
from pygame import Surface
from pygame.math import Vector2

@dataclass
class Entity:
    SLOWDOWN_TIME = 0.2
    SLOWDOWN_FACTOR = 0.5
    position: Vector2 = Vector2()
    motion: Vector2 = Vector2()
    force: Vector2 = Vector2()
    offset: Vector2 = Vector2()
    sprite: Surface = None
    size: float = 0.0
    hit_points: int = 1
    debris: bool = False
    slowdown: float = 0.0
