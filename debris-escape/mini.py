# import the pygame module, so you can use it
import pygame
import os
 
FRAME_TIME = 1.0/60.0

# define a main function
def main():
     
    # initialize the pygame module
    pygame.init()
    # load and set the logo
    #logo = pygame.image.load("logo32x32.png")
    #pygame.display.set_icon(logo)
    pygame.display.set_caption("minimal program")
     
    # create a surface on screen that has the size of 240 x 180
    screen = pygame.display.set_mode((640,360))
    sprites = pygame.sprite.Group()
    fighter = pygame.sprite.Sprite(sprites)
    fighter.image = pygame.image.load(os.path.join("assets", "fighter.png")).convert_alpha()
    fighter.rect = pygame.Rect((20, 20), (8, 8))
    pos = pygame.Vector2(20, 20)
     
    # define a variable to control the main loop
    running = True
    last_tick = pygame.time.get_ticks()
     
    # main loop
    while running:
        current_tick = pygame.time.get_ticks()
        delta = (current_tick - last_tick) / 1000.0
        last_tick = current_tick
        # event handling, gets all event from the event queue
        for event in pygame.event.get():
            # only do something if the event is of type QUIT
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                running = False
        pos.x += 10 * delta
        fighter.rect.topleft = pos
        screen.fill((0,0,0))
        sprites.draw(screen)
        pygame.display.flip()
     
     
# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()
